import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_xd_foodapp/src/widgets/BoughtFood.dart';
import 'package:flutter_xd_foodapp/src/widgets/FoodCategory.dart';
import 'package:flutter_xd_foodapp/src/widgets/HeaderTopInfo.dart';
import 'package:flutter_xd_foodapp/src/widgets/SearchField.dart';
import 'data/FoodData.dart';

class HomeScreen extends StatefulWidget{
  @override
  _HomeScreenState createState()=> _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>{
  List<Food> _foods = foods;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       body: ListView(
         padding: EdgeInsets.only(top:50.0,left:20.0,right:20.0),
          children: [
            HeaderTopinfo(),
            FoodCategory(),
            SizedBox(height: 30.0,),
            SearchField(),
            SizedBox(height: 20.0,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Freqently Bought Foods",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18.0),),
                Text("View All",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18.0,color: Colors.orange),)
              ],
            ),
            SizedBox(height: 20.0,),
           Column(
             children: _foods.map(_BoughtItems).toList()
           ),
          ],
       ),
    );
  }
  Widget _BoughtItems(Food food){
    return Container(
      margin: EdgeInsets.only(bottom: 20.0),
      child: BoughtFood(
          id:food.id,
          name:food.name,
          imagePath:food.imagePath,
          category:food.category,
          price:food.price,
          discount:food.discount,
          ratings:food.ratings,
      ),
    );
  }
}

