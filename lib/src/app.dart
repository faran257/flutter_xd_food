import 'package:flutter/material.dart';
import 'package:flutter_xd_foodapp/src/HomeScreen.dart';


class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Food Delivery App',
      theme: ThemeData(
        primaryColor: Colors.blueAccent
      ),
      home:HomeScreen(),
    );
  }
}
