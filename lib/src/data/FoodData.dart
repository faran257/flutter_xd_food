class Food{
  final String id;
  final String name;
  final String imagePath;
  final String category;
  final String price;
  final String discount;
  final String ratings;
  Food({this.id,this.name,this.imagePath,this.category,this.price,this.discount,this.ratings});
}

final foods =[
  Food(
    id:"1",
    name:"CoffeeCoo",
    imagePath:"assets/images/breakfast.jpg",
    category:"1",
    price:"150.0",
    discount:"20.0",
    ratings:"1",
  ),
  Food(
    id:"2",
    name:"Biryani",
    imagePath:"assets/images/biryani.png",
    category:"1",
    price:"180.0",
    discount:"20.0",
    ratings:"3",
  ),
  Food(
    id:"3",
    name:"Burger",
    imagePath:"assets/images/burger.png",
    category:"1",
    price:"180.0",
    discount:"20.0",
    ratings:"3",
  ),
];