import 'package:flutter/material.dart';


  class BoughtFood extends StatefulWidget{
    final String id;
    final String name;
    final String imagePath;
    final String category;
    final String price;
    final String discount;
    final String ratings;

    BoughtFood({this.id,this.name,this.imagePath,this.category,this.price,this.discount,this.ratings});
  @override
  _BoughtFoodState createState()=> _BoughtFoodState();
  }

  class _BoughtFoodState extends State<BoughtFood>{

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 200,
          width: 340,
          child: Image.asset(widget.imagePath,fit:BoxFit.cover),
        ),
        Positioned(
           left: 0.0,
          bottom: 0.0,
          child: Container(
            height: 50.0,
            width: 340.0,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.black,Colors.black12]
                )
            ),
          ),
        ),
       Positioned(
          left: 10.0,
          bottom: 10.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: [
               Column(
                 crossAxisAlignment: CrossAxisAlignment.start,
                 children: [
                   Text(widget.name,style: TextStyle(color:Colors.white,fontSize: 16.0,fontWeight: FontWeight.bold),),
                   Row(
                     children: [
                       Icon(Icons.star,color:Theme.of(context).primaryColor,size:16.0),
                       Icon(Icons.star,color:Theme.of(context).primaryColor,size:16.0),
                       Icon(Icons.star,color:Theme.of(context).primaryColor,size:16.0),
                       Icon(Icons.star,color:Theme.of(context).primaryColor,size:16.0),
                       Icon(Icons.star,color:Theme.of(context).primaryColor,size:16.0),
                       SizedBox(width: 20.0,),
                       Text("27 reviews",style: TextStyle(color:Colors.grey),),
                       SizedBox(width: 30.0,),
                       Text("Rs=100.0",style: TextStyle(color:Colors.orange,fontSize: 14.0,fontWeight:FontWeight.bold),),
                     ],
                   ),
                 ],
               ),
             ],
           )
       ),
      ],
    );
  }
}