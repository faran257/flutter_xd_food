import 'package:flutter/material.dart';

class HeaderTopinfo extends StatelessWidget{
  final textStyle = TextStyle(fontSize:32.0,fontWeight: FontWeight.bold);
  @override
  Widget build(BuildContext context) {
    return  Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("What Would", style: textStyle),
            Text("you like to eat?", style: textStyle)
          ],
        ),
        Icon(Icons.notifications_none,size: 40.0,color:Theme.of(context).primaryColor),

      ],
    );
  }
}