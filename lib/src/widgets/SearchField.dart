import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_xd_foodapp/src/model/CategoryModel.dart';


class SearchField extends StatelessWidget{
  @override
  Widget build(BuildContext context){
     return Material(
        elevation: 5.0,
        borderRadius:BorderRadius.circular(30.0),
        child: TextField(
           decoration: InputDecoration(
             contentPadding: EdgeInsets.symmetric(horizontal: 30.0,vertical: 15.0),
             hintText: "Search now",
             suffixIcon: Icon(Icons.search,color: Colors.black,),
             // prefixIcon: Icon(Icons.search)
             border: InputBorder.none
           ),
         ),
     );
  }
}